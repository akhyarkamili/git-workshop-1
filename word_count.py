import operator


def count(file):
    dict_container = {}
    text_container = file.split()
    for text in text_container:
        if text in dict_container:
            dict_container[text] += 1
        else:
            dict_container[text] = 1
    dict_container = dict(sorted(dict_container.items(), key=operator.itemgetter(1), reverse=True))
    return dict_container
